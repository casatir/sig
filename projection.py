#!/usr/bin/env python3

from pyproj import Transformer
import sys
import json
import gzip

# Les fichiers passés par la ligne de commande sont convertis
# en Lambert93.


transformer = Transformer.from_crs(4326, 2154)


def project(points):
    x = [p[0] for p in points]
    y = [p[1] for p in points]
    return list(zip(*transformer.transform(x, y)))


for f in sys.argv[1:]:
    data = json.load(gzip.open(f))
    if isinstance(data[0][0], float):
        data = project(data)
    else:
        data = [project(d) for d in data]
    json.dump(data, gzip.open(f.replace('.json.gz', '.proj.json.gz'), 'wt'))
